const helpers = require('../Instructions/StarterCode/static/js/helpers.js');

describe("helpers", () => {
    describe("validateData", () => {
        it("should not throw an error if the id arrays are not all the same. ", () => {
            helpers.validateData({
                metadata: [{id: 1}],
                names: [1],
                samples: [{id: 1}]
            });
        });

        it("should throw an error if the id arrays are not all the same length. ", () => {
            expect(() => {
                helpers.validateData({
                    metadata: [{id: 2}],
                    names: [],
                    samples: [{id: 1}]
                });
            }).toThrow(new Error("The identifiers provided in the data do not match. "));
        });

        it("should throw an error if the id arrays do not have the same values. ", () => {
            expect(() => {
                helpers.validateData({
                    metadata: [{id: 2}],
                    names: [2],
                    samples: [{id: 1}]
                });
            }).toThrow(new Error("The identifiers provided in the data do not match. "));
        });
    });

    describe("cacheDecorator", () => {
        it("should return cached value for same input. ", () => {
            const a = [];
            let runs = 0;

            const cache = new Map();
            const func = helpers.cacheDecorator(foo, cache);

            func(a);
            const result = func(a);

            expect(result).toBe(1);

            function foo(i){
                runs += 1;
                return runs;
            }
        });
    });

    describe("takeFromZipped", () => {
        it("should take all if there are less than amount. ", () => {
            const zipped = [{
                sample_values: 1,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 4,
                otu_ids: 5,
                otu_labels: 6,
            }];
            const result = [...helpers.takeFromZipped(zipped, 3)];
            expect(result.length).toEqual(2);
        });

        it("should take only the number specified in amount. ", () => {
            const zipped = [{
                sample_values: 1,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 4,
                otu_ids: 5,
                otu_labels: 6,
            }];
            const result = [...helpers.takeFromZipped(zipped, 1)];
            expect(result.length).toEqual(1);
        });

        it("should take the highest sample values first. ", () => {
            const zipped = [{
                sample_values: 2,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 7,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 1,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 4,
                otu_ids: 5,
                otu_labels: 6,
            }];
            const result = helpers.takeFromZipped(zipped, 2).map(d => d.sample_values);
            expect(result).toEqual([7, 4]);
        });
    });

    describe("zipSample", () => {
        it("should zip samples in order. ", () => {
            const expected = [{
                sample_values: 1,
                otu_ids: 2,
                otu_labels: 3,
            },{
                sample_values: 4,
                otu_ids: 5,
                otu_labels: 6,
            }];

            const result = helpers.zipSample({
                sample_values:[1, 4],
                otu_ids: [2, 5],
                otu_labels: [3, 6],
            });

            expect(Array.from(result)).toEqual(expected);
        });

        it("should throw an error if the lengths do not match. ", () => {
            expect(() => {
                Array.from(...helpers.zipSample({
                    sample_values:[],
                    otu_ids: [],
                    otu_labels: [1],
                }));
            }).toThrow(new Error("The lengths of sample values, OTU IDs, and OTU Labels do not match for this sample. "));

            expect(() => {
                Array.from(...helpers.zipSample({
                    sample_values:[2],
                    otu_ids: [],
                    otu_labels: [],
                }));
            }).toThrow(new Error("The lengths of sample values, OTU IDs, and OTU Labels do not match for this sample. "));

        });
    });
});